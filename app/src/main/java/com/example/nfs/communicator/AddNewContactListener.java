package com.example.nfs.communicator;

import com.example.nfs.model.Contact;

public interface AddNewContactListener {
    void respond();
    void respondAddNewContact(Contact contact);
    void respondEditContact(Contact contact);
    void respondUpdateContact(Contact contact);
    void respondDeleteContact(Contact contact);
}
