package com.example.nfs.model;

import com.google.gson.annotations.SerializedName;

public class ImageUploaded {

    @SerializedName("msg")
    private String msg;
    @SerializedName("uploadid")
    private String uploadId;
    @SerializedName("ids")
    private String[] ids;

    public ImageUploaded(String msg, String uploadId, String[] ids) {
        this.msg = msg;
        this.uploadId = uploadId;
        this.ids = ids;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getUploadId() {
        return uploadId;
    }

    public void setUploadId(String uploadId) {
        this.uploadId = uploadId;
    }

    public String[] getIds() {
        return ids;
    }

    public void setIds(String[] ids) {
        this.ids = ids;
    }
}
