package com.example.nfs.web_service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitNetworkService {

//    private static final String BASE_URL = "https://stdevtask3-0510.restdb.io/media/5acc5fa4836221430001d053/";
    private static final String BASE_URL = "https://stdevtask3-0510.restdb.io/rest/";

    private static Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();

    public static <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}
