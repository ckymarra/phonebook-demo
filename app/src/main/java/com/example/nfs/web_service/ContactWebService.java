package com.example.nfs.web_service;

import com.example.nfs.model.Contact;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ContactWebService {
    @GET("contacts")
    Call<List<Contact>> getContacts(@Header("apiKey") String apiKey);

    //    Call<List<Contact>> getContacts(@Query("q") String q, @Query("h") String h);
    @POST("contacts")
    Call<Contact> addNewContact(@Header("apiKey") String apiKey, @Body Contact contact);

    @PATCH("contacts/{objectid}")
    Call<Contact> updateContact(@Header("apiKey") String apiKey, @Path("objectid") String objectId, @Body Contact contact);

    @DELETE("contacts/{objectid}")
    Call<Void> deleteContact(@Header("apiKey") String apiKey, @Path("objectid") String objectId);

//    @FormUrlEncoded
//    @POST("contacts")
//    Call<Contact> addNewContactBroken
//            (@Header(("apiKey")) String apiKey,
//             @Field("firstName") String firstName,
//             @Field("lastName") String lastName,
//             @Field("phone") String phone,
//             @Field("email") String mail,
//             @Field("notes") String notes,
//             @Field("images") String[] images
//            );
}
