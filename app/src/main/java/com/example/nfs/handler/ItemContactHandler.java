package com.example.nfs.handler;

import android.view.View;

public interface ItemContactHandler {
    void onItemContactClicked(View view);
}
