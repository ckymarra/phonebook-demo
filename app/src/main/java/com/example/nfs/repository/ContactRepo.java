package com.example.nfs.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.nfs.model.Contact;
import com.example.nfs.web_service.ContactWebService;
import com.example.nfs.web_service.RetrofitNetworkService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactRepo {

    private static ContactRepo contactRepo;
    private static ContactWebService contactWebService;
    private MutableLiveData<Boolean> isLoading = new MutableLiveData<>();

    private static final String TAG = "nfs.CONTACT_REPO";


    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    private ContactRepo(){
        contactWebService = RetrofitNetworkService.createService(ContactWebService.class);
    }

    public static ContactRepo getInstance(){
        if (contactRepo == null){
            contactRepo = new ContactRepo();
        }
        return contactRepo;
    }

    public MutableLiveData<List<Contact>> getContacts(String apiKey){
        isLoading.setValue(true);
        final MutableLiveData<List<Contact>> allContacts = new MutableLiveData<>();

        contactWebService.getContacts(apiKey).enqueue(new Callback<List<Contact>>() {
            @Override
            public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {
                if (response.isSuccessful()){
                    allContacts.postValue(response.body());
                    isLoading.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<List<Contact>> call, Throwable t) {
                allContacts.postValue(null);
                isLoading.setValue(false);
                t.printStackTrace();
            }
        });
        return allContacts;
    }

    public void addNewContact(String apiKey, final Contact contact){

        contactWebService.addNewContact(apiKey, contact).enqueue(new Callback<Contact>() {
            @Override
            public void onResponse(Call<Contact> call, Response<Contact> response) {
                if (response.isSuccessful()){
                    Log.d(TAG, "add response: " + response.body().toString());
                    contact.setId(response.body().getId());
                }
            }

            @Override
            public void onFailure(Call<Contact> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void updateContact(String apiKey, String objectId, Contact contact){
        contactWebService.updateContact(apiKey, objectId, contact).enqueue(new Callback<Contact>() {
            @Override
            public void onResponse(Call<Contact> call, Response<Contact> response) {
                if (response.isSuccessful()){
                    Log.d(TAG, "update response: " + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Contact> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void deleteContact(String apiKey, String objectId){
        contactWebService.deleteContact(apiKey, objectId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(TAG, "delete response");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
