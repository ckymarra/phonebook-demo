package com.example.nfs.fragment;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nfs.R;
import com.example.nfs.communicator.AddNewContactListener;
import com.example.nfs.model.Contact;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddEditContactFragment extends Fragment {

    private EditText etvFirstName;
    private EditText etvLastName;
    private EditText etvPhoneNumber;
    private EditText etvEmail;
    private EditText etvNotes;
    private Button btnSave;
    private Button btnDelete;
    private Contact contact;

    private AddNewContactListener listener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (AddNewContactListener) context;
    }

    public AddEditContactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_edit_contact, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewsById(view);
        setOnClickListeners();
        checkIfContactAlreadyExists();

    }

    private void findViewsById(View v) {
        etvFirstName = v.findViewById(R.id.etv_first_name);
        etvLastName = v.findViewById(R.id.etv_last_name);
        etvPhoneNumber = v.findViewById(R.id.etv_phone_number);
        etvEmail = v.findViewById(R.id.etv_mail);
        etvNotes = v.findViewById(R.id.etv_notes);
        btnSave = v.findViewById(R.id.btn_save);
        btnDelete = v.findViewById(R.id.btn_delete);
    }

    private void setOnClickListeners() {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String firstName = etvFirstName.getText().toString();
                String lastName = etvLastName.getText().toString();
                String phoneNumber = etvPhoneNumber.getText().toString();
                String mail = etvEmail.getText().toString();
                String notes = etvNotes.getText().toString();

                if (firstName.trim().isEmpty() ||
                        lastName.trim().isEmpty() ||
                        phoneNumber.trim().isEmpty() ||
                        mail.trim().isEmpty() ||
                        notes.trim().isEmpty()) {
                    Toast.makeText(getActivity(), "Please, make sure that all fields are filled", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!mail.trim().contains("@") || !mail.trim().contains(".")) {
                    Toast.makeText(getActivity(), "Mail should consist of @ symbol and a dot", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (contact != null) {
                    contact.setFirstName(firstName);
                    contact.setLastName(lastName);
                    contact.setPhone(phoneNumber);
                    contact.setEmail(mail);
                    contact.setNotes(notes);
                    listener.respondUpdateContact(contact);
                } else {
                    Contact newContact = new Contact(firstName, lastName, phoneNumber, mail, notes, null);
                    listener.respondAddNewContact(newContact);
                }
                getActivity().onBackPressed();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contact == null){
                    Toast.makeText(getActivity(), "Cannot delete unsaved contact", Toast.LENGTH_SHORT).show();
                    return;
                }
                listener.respondDeleteContact(contact);
                getActivity().onBackPressed();
            }
        });
    }

    private void checkIfContactAlreadyExists(){
        if (contact != null){
            etvFirstName.setText(contact.getFirstName());
            etvLastName.setText(contact.getLastName());
            etvPhoneNumber.setText(contact.getPhone());
            etvEmail.setText(contact.getEmail());
            etvNotes.setText(contact.getNotes());
        }
    }

    //Setters
    public void setContact(Contact contact) {
        this.contact = contact;
    }


}
