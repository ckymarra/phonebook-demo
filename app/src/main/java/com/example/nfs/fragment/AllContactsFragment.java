package com.example.nfs.fragment;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.nfs.R;
import com.example.nfs.adapter.ContactAdapter;
import com.example.nfs.communicator.AddNewContactListener;
import com.example.nfs.model.Contact;
import com.example.nfs.view_model.ViewModelContact;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllContactsFragment extends Fragment {

    private RecyclerView recyclerView;
    private FrameLayout loadingBarContainer;
    private ViewModelContact viewModelContact;
    private FloatingActionButton fbtnAdd;
    private AddNewContactListener listener;
    private ContactAdapter adapter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (AddNewContactListener) context;
    }

    public AllContactsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_all_contacts, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewsById(view);
        setOnClickListeners();
        setupWithViewModel();
    }

    private void findViewsById(View view){
        recyclerView = view.findViewById(R.id.general_contact_cardview_all_contacts);
        loadingBarContainer = view.findViewById(R.id.progress_bar_container_all_contacts);
        fbtnAdd = view.findViewById(R.id.fbtn_add);
    }

    private void setOnClickListeners(){
        fbtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.respond();
            }
        });
    }

    private void setupWithViewModel() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        initializeViewModel();

        viewModelContact.getIsLoading().observe(getActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    showProgress();
                } else {
                    hideProgress();
                }
            }
        });

        viewModelContact.getAllContacts().observe(getActivity(), new Observer<List<Contact>>() {
            @Override
            public void onChanged(List<Contact> contacts) {
                adapter = new ContactAdapter(getActivity());
                adapter.setContactList(contacts);
                recyclerView.setAdapter(adapter);
            }
        });
    }

    private void initializeViewModel() {
        viewModelContact = ViewModelProviders.of(this).get(ViewModelContact.class);
        viewModelContact.initialize();
    }



    /********** Public methods ************/

    public void notifyAddedNewContact(Contact contact){
        viewModelContact.addNewContact(contact);
        adapter.notifyDataSetChanged();
    }

    public void notifyUpdatedContact(Contact contact){
        viewModelContact.updateContact(contact.getId(), contact);
        adapter.notifyDataSetChanged();
    }

    public void notifyDeleteContact(Contact contact){
        viewModelContact.deleteContact(contact.getId());
        adapter.notifyDataSetChanged();
    }

    //Loading Bar stuff

    private void showProgress(){
        loadingBarContainer.setVisibility(View.VISIBLE);
    }

    private void hideProgress(){
        loadingBarContainer.setVisibility(View.GONE);
    }

}

