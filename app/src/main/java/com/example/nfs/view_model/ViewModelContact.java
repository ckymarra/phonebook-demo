package com.example.nfs.view_model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.nfs.model.Contact;
import com.example.nfs.repository.ContactRepo;

import java.util.List;

public class ViewModelContact  extends ViewModel {

    private ContactRepo contactRepo;
    private static final String API_KEY = "a5b39dedacbffd95e1421020dae7c8b5ac3cc";

    public void initialize(){
        contactRepo = ContactRepo.getInstance();
    }

    public MutableLiveData<List<Contact>> getAllContacts(){
        return contactRepo.getContacts(API_KEY);
    }

    public MutableLiveData<Boolean> getIsLoading(){
        return contactRepo.getIsLoading();
    }

    public void addNewContact(Contact contact){
        contactRepo.addNewContact(API_KEY, contact);
    }

    public void updateContact(String objectId, Contact contact){
        contactRepo.updateContact(API_KEY, objectId, contact);
    }

    public void deleteContact(String objectId){
        contactRepo.deleteContact(API_KEY, objectId);
    }


}
