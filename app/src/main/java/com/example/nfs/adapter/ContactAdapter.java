package com.example.nfs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nfs.communicator.AddNewContactListener;
import com.example.nfs.databinding.ItemContactBinding;
import com.example.nfs.handler.ItemContactHandler;
import com.example.nfs.model.Contact;

import java.util.ArrayList;
import java.util.List;


public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder>{

    private LayoutInflater inflater;
    private Context context;
    private List<Contact> contacts;
    private AddNewContactListener listener;

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        listener = (AddNewContactListener) context;
    }

    public ContactAdapter(Context context){
        this.contacts = new ArrayList<>();
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void setContactList(List<Contact> contactList){
        this.contacts.clear();
        if(contactList != null){
            this.contacts.addAll(contactList);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ContactAdapter.ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemContactBinding binding = ItemContactBinding.inflate(inflater, parent, false);
        return new ContactViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ContactAdapter.ContactViewHolder holder, final int position) {
        holder.binding.setContact(contacts.get(position));
        holder.binding.setItemContactHandler(new ItemContactHandler() {
            @Override
            public void onItemContactClicked(View view) {
                listener.respondEditContact(holder.binding.getContact());
            }
        });

    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public Contact getContactAt(int position) {
        return contacts.get(position);
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder{

        ItemContactBinding binding;


        public ContactViewHolder(ItemContactBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
