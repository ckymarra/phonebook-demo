package com.example.nfs.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;


import android.os.Bundle;
import android.widget.Toast;

import com.example.nfs.R;
import com.example.nfs.communicator.AddNewContactListener;
import com.example.nfs.fragment.AddEditContactFragment;
import com.example.nfs.fragment.AllContactsFragment;
import com.example.nfs.model.Contact;


public class MainActivity extends AppCompatActivity implements AddNewContactListener {

    private AllContactsFragment allContactsFragment;
    private AddEditContactFragment addEditContactFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        allContactsFragment = new AllContactsFragment();

        changeFragment(allContactsFragment, null);

    }

    private void changeFragment(Fragment fragment, String name) {
        if (name == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.addToBackStack(null);
            transaction.replace(R.id.frame_container, fragment).commit();
        } else {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.addToBackStack(name);
            transaction.replace(R.id.frame_container, fragment).commit();
        }
    }

    //inter fragment callbacks

    @Override
    public void respond() {
        changeFragment(new AddEditContactFragment(), null);
    }

    @Override
    public void respondAddNewContact(Contact contact) {
        allContactsFragment.notifyAddedNewContact(contact);
        Toast.makeText(MainActivity.this, "Contact created", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void respondEditContact(Contact contact) {
        addEditContactFragment = new AddEditContactFragment();
        addEditContactFragment.setContact(contact);
        changeFragment(addEditContactFragment, null);
    }

    @Override
    public void respondUpdateContact(Contact contact) {
        allContactsFragment.notifyUpdatedContact(contact);
        Toast.makeText(MainActivity.this, "Contact updated", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void respondDeleteContact(Contact contact) {
        allContactsFragment.notifyDeleteContact(contact);
        Toast.makeText(MainActivity.this, "Contact deleted", Toast.LENGTH_SHORT).show();
    }
}
